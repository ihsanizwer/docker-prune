#!/bin/bash
clear
tput bold; cat header; tput sgr0
echo ""
echo "______________________________________________________________________________"
echo ""
tput bold; echo "This is a simple script that can remove unncessary temporaray files "
echo "Please note that all dangling containers will be removed. "
echo "If there are any images that need to ignored, run them before proceeding."; tput sgr0
echo "______________________________________________________________________________"
echo ""
echo "Author: Ihsan Izwer                                              Version: 1.0"
echo "______________________________________________________________________________"
echo ""
docker system prune
#input arguments to include other things like deleting volumes etc.